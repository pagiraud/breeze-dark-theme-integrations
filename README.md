# Breeze Dark Theme Integrations

This is a place where I will store some configuration files enhancing the integration of programs that I use with the KDE Plasma Breeze Dark Theme.

To begin with, please ensure you have selected Breeze Dark Theme for GNOME/GTK applications.

## GnuCash

Just put the CSS file in `$HOME/.config/gnucash`

## Mozilla Thunderbird

I have built upon [this repository](https://gitlab.com/raginggoblin/thunderbird-breeze). Follow its instructions, and then replace its `userChrome.css` with mine.

## TeXstudio

It's a Qt application, but unusable as is with Breeze Dark Theme. With default style and Classic color scheme selected in the application, replace the [formats] section of your `$HOME/.config/texstudio/texstudio.ini` with mine. *Do not* override the rest of the file. This is just a copy of [this gist](https://gist.github.com/carmelom/50f18f2d9c95e76edc75968cf89df473).

## Okular

It is a very well integrated KDE Plasma application, but I feel it can do better when it comes to dark mode. There is no dedicated file for it in this repo, just the following tip :
1. go to Settings > Configure Okular
2. go to the accessibility panel
3. check Change Colors and then select Change Dark & Light Colors
4. As dark color, choose white but as light color, choose `#1d2023` (this is a dark color used by Breeze Dark Theme)
5. To easily switch between normal colors and accessibility colors, you can add a button to your toolbar : Go to Settings > Configure Toolbar. Select Toggle colors. You can even rename it to something as Night Mode.
